package conformance;

import implementation.EntityManager;
import petrinet.EventLog;
import petrinet.PetriNet;

public class Controller {
	
	public static void main(String[] arguments) throws Exception {
		
		EntityManager entityManager = new EntityManager();
		PetriNet petriNet = entityManager.getPetriNet(arguments[0]);
		EventLog eventLog = entityManager.getEventLog(arguments[1]);
		
		ConformanceChecker conformanceChecker = new ConformanceChecker(eventLog, petriNet);
		
		double fitness = conformanceChecker.getFitness();
		double simpleBehavioralAppropriateness = conformanceChecker.getSimpleBehavAppropr();
		double simpleStructuralAppropriateness = conformanceChecker.getSimpleStructAppropr();
		
		System.out.println("fitness: " + String.valueOf(fitness));
		System.out.println("simple behavioral appropriateness: " + String.valueOf(simpleBehavioralAppropriateness));
		System.out.println("simple structural appropriateness: " + String.valueOf(simpleStructuralAppropriateness));
	}
}
