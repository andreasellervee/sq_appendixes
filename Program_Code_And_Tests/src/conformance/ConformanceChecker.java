package conformance;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import petrinet.Event;
import petrinet.EventLog;
import petrinet.PetriNet;
import petrinet.Place;
import petrinet.Trace;
import petrinet.Transition;

public class ConformanceChecker {
	
	private PetriNet petriNet;
	
	private EventLog eventLog;
	
	public ConformanceChecker(EventLog log, PetriNet net) {
		this.eventLog = log;
		this.petriNet = net;
		
		for(Trace trace : this.eventLog.getTraces()) {
			trace.addProducedToken();
			// getNextTrace
			for(Event event : trace.getEvents()) {
				// getNextEvent
				countEnabledEventsBeforeConsumingAnEvent(trace);
				for(Transition transition : this.petriNet.getTransitions()) {
					// getNextTransition
					if(transition.getName().equals(event.getName())) {
						transition.setTrace(trace);
						// consume the event
						transition.consumeEvent();
					}
				}
			}
			
			// check for remaining tokens in the PetriNet
			for(Place place : this.petriNet.getPlaces()) {
				// getNextPlace
				if(remainingTokenFound(place)) {
					// if there is a remaining token found add to the counter
					trace.addRemainingToken();
				}
			}
			trace.addConsumedToken();
			// reset the petri net for new trace
			this.petriNet.reset();
		}
	}

	public PetriNet getPetriNet() {
		return petriNet;
	}

	public EventLog getEventLog() {
		return eventLog;
	}
	
	public double getFitness() {
		List<Trace> traces = getEventLog().getTraces();
		Map<String, Integer> traceInstances = new HashMap<>();
		Map<String, Trace> something = new HashMap<>();
		for(Trace trace : traces) {
			if (traceInstances.containsKey(trace.toString())) {
				traceInstances.put(trace.toString(), traceInstances.get(trace.toString()) + 1);
			} else {
				traceInstances.put(trace.toString(), 1);
				something.put(trace.toString(), trace);
			}
		}

		double nTimesM = 0;
		double nTimesR = 0;
		double nTimesC = 0;
		double nTimesP = 0;
		
		Collection<String> differentTraces = traceInstances.keySet();
		
		for(String stringTrace : differentTraces) {
			Trace trace = something.get(stringTrace);
			nTimesM += trace.getNumber_of_instances() * trace.getMissing_tokens();
			nTimesR += trace.getNumber_of_instances() * trace.getRemaining_tokens();
			nTimesC += trace.getNumber_of_instances() * trace.getConsumed_tokens();
			nTimesP += trace.getNumber_of_instances() * trace.getProduced_tokens();
		}
		
		double fitness = (0.5 * (1 - (nTimesM / nTimesC))) + (0.5 * (1 - (nTimesR / nTimesP)));
		return fitness;
	}
	
	public double getSimpleBehavAppropr() {
		List<Trace> traces = getEventLog().getTraces();
		Map<String, Integer> traceInstances = new HashMap<>();
		Map<String, Trace> something = new HashMap<>();
		for(Trace trace : traces) {
			if (traceInstances.containsKey(trace.toString())) {
				traceInstances.put(trace.toString(), traceInstances.get(trace.toString()) + 1);
			} else {
				traceInstances.put(trace.toString(), 1);
				something.put(trace.toString(), trace);
			}
		}
		// number of visible tasks
		int visibleTasks = getPetriNet().getTransitions().size();
		double sum1 = 0;
		double sum2 = 0;
		for(String stringTrace : traceInstances.keySet()) {
			Trace trace = something.get(stringTrace);
			sum1 += trace.getNumber_of_instances() * (visibleTasks - ((double) trace.getTotal_enabled_transition_count() / (double) trace.getEvents().size()));
		}
		for(String trace : traceInstances.keySet()) {
			Trace correspondingTrace = something.get(trace);
			sum2 += correspondingTrace.getNumber_of_instances();
		}
		return sum1 / ((visibleTasks - 1) * sum2);
	}
	
	public double getSimpleStructAppropr() {
		// number of distinct labels in the Event Log
		double L = getEventLog().getDistinctLabels().size();
		// total number of places + transitions
		double N = getPetriNet().getPlaces().size() + getPetriNet().getTransitions().size();
		return (L+2)/N;
	}
	
	private boolean remainingTokenFound(Place place) {
		return (place.getNumberOfTokens() > 0) && (place.isFinalPlace() == false);
	}

	private void countEnabledEventsBeforeConsumingAnEvent(Trace trace) {
		for(Transition transition : this.petriNet.getTransitions()) {
			transition.setTrace(null);
			if(transition.isEnabled()) {
				trace.addToTotalNumberOfEnabledTransitions();
			}
		}
	}


}
