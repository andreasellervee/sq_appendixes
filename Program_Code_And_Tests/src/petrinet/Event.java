package petrinet;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Event {
	
	private String name;
	private Date timestamp;
	private Map<String, Object> eventAttributes = new HashMap<String, Object>();
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	public Map<String, Object> getEventAttributes() {
		return eventAttributes;
	}
	public void setEventAttributes(Map<String, Object> eventAttributes) {
		this.eventAttributes = eventAttributes;
	}	
	
}
