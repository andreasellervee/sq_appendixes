package petrinet;

import java.util.List;

public class PetriNet {
	
	private List<Place> places;
	private List<Transition> transitions;


	public void reset() {
		for(Place place : getPlaces()) {
			if (place.isInitalPlace()) {
				place.setNumberOfTokens(1);
			} else {
				place.setNumberOfTokens(0);
			}
		}
		for(Transition transition : getTransitions()) {
			for(Place place : transition.getInputPlaces()) {
				if (place.isInitalPlace()) {
					place.setNumberOfTokens(1);
				} else {
					place.setNumberOfTokens(0);
				}
			}
			for(Place place : transition.getOutputPlaces()) {
				place.setNumberOfTokens(0);
			}
		}
	}
	
	public List<Place> getPlaces() {
		return places;
	}
	
	public void setPlaces(List<Place> places) {
		this.places = places;
	}
	
	public List<Transition> getTransitions() {
		return transitions;
	}
	
	public void setTransitions(List<Transition> transitions) {
		this.transitions = transitions;
	}
}
