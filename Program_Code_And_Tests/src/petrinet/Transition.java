package petrinet;

import java.util.Iterator;
import java.util.List;

public class Transition {
	
	public Transition() {
	}
	
	private Trace trace;
	
	public void setTrace(Trace trace) {
		this.trace = trace;
	}
	

	private String name;
	private List<Place> inputPlaces;
	private List<Place> outputPlaces;
	
	public void consumeEvent() {
		// check if the transition is enabled
		isEnabled();
		fire();
	}
	
	private void fire(){
		// consume the tokens
		consumeTokens();
		// produce the tokens
		produceTokens();
	}
	
	private void consumeTokens(){
		// iterate over all input places
		Iterator<Place> inputIterator = getInputPlaces().iterator();
		while(inputIterator.hasNext()) {
			Place nextInputPlace = inputIterator.next();
			// Consume a token from the input place
			nextInputPlace.consumeToken();
			trace.addConsumedToken();
		}
	}
	
	private void produceTokens() {
		// iterate over all output places
		Iterator<Place> outputIterator = getOutputPlaces().iterator();
		while(outputIterator.hasNext()) {
			Place nextOutputPlace = outputIterator.next();
			// Produce a token to the output Place
			nextOutputPlace.produceToken();
			trace.addProducedToken();
		}
	}
	
	public boolean isEnabled() {
		// let's iterate (loop) over all the inputPlaces
		boolean transitionEnabled = true;
		Iterator<Place> inputPlaceIterator = getInputPlaces().iterator();
		while(inputPlaceIterator.hasNext()) {
			Place nextInputPlace = inputPlaceIterator.next();
			// if inputPlace is missing a token
			if (nextInputPlace.getNumberOfTokens() < 1) {
				// return false because one
				// of the input places is missing a token
				transitionEnabled = false;
				if(trace != null) {
					trace.addMissingToken();
				}
			}
		}
		if(transitionEnabled && trace != null) {
			trace.addEnabledTransition();
		}
		return transitionEnabled;
	}
	
	public void setLabel(String label) {
		this.name = label;
	}

	public void setInputPlaces(List<Place> inputPlaces) {
		this.inputPlaces = inputPlaces;
	}

	public void setOutputPlaces(List<Place> outputPlaces) {
		this.outputPlaces = outputPlaces;
	}
	
	public String getName() {
		return name;
	}
	public List<Place> getInputPlaces(){
		return inputPlaces;
	}
	public List<Place> getOutputPlaces() {
		return outputPlaces;
	}

}
