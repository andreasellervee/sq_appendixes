package petrinet;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import petrinet.Trace;

public class EventLog {
	
	private List<Trace> traces = new ArrayList<Trace>();

	public List<Trace> getTraces() {
		return traces;
	}

	public void setTraces(List<Trace> traces) {
		this.traces = traces;
	}
	
	public Set<String> getDistinctLabels() {
		Set<String> eventsInLog = new HashSet<String>();
		for(Trace trace : getTraces()) {
			for(Event event : trace.getEvents()) {
				eventsInLog.add(event.getName());
			}
		}
		return eventsInLog;
	}
}
