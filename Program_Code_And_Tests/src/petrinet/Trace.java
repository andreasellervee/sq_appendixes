package petrinet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Trace {
	
	private int caseId;
	private List<Event> events = new ArrayList<Event>();
	private Map<String, Object> caseAttributes = new HashMap<String, Object>();

	// counters
	private int missing_tokens = 0;
	private int remaining_tokens = 0;
	private int consumed_tokens = 0;
	private int produced_tokens = 0;
	private int enabled_transitions = 0;
	private int number_of_instances = 0;
	
	// for the mean number of transitions calculation
	private int total_enabled_transition_count = 0;
	
	// getter methods for the counters
	public int getMissing_tokens() {
		return missing_tokens;
	}
	public int getRemaining_tokens() {
		return remaining_tokens;
	}
	public int getConsumed_tokens() {
		return consumed_tokens;
	}
	public int getProduced_tokens() {
		return produced_tokens;
	}
	public int getEnabled_transitions() {
		return enabled_transitions;
	}
	public int getNumber_of_instances() {
		return number_of_instances;
	}
	
	// updating the counters
	public void addMissingToken() {
		missing_tokens += 1;
	}
	public void addRemainingToken() {
		remaining_tokens += 1;
	}
	public void addConsumedToken() {
		consumed_tokens += 1;
	}
	public void addProducedToken() {
		produced_tokens += 1;
	}
	public void addEnabledTransition() {
		enabled_transitions += 1;
	}
	public void setNumberOfInstances(int value) {
		number_of_instances = value;
	}
	public void addToTotalNumberOfEnabledTransitions() {
		total_enabled_transition_count += 1;
	}
	
	public int getTotal_enabled_transition_count() {
		return total_enabled_transition_count;
	}
	
	public int getCaseId() {
		return caseId;
	}
	public void setCaseId(int caseId) {
		this.caseId = caseId;
	}
	
	public List<Event> getEvents() {
		return events;
	}
	public void setEvents(List<Event> events) {
		this.events = events;
	}
	
	public Map<String, Object> getCaseAttributes() {
		return caseAttributes;
	}
	public void setCaseAttributes(Map<String, Object> caseAttributes) {
		this.caseAttributes = caseAttributes;
	}

}
