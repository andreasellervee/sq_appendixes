package petrinet;

public class Place {
	
	private String name;
	private int numberOfTokens;
	
	public void consumeToken() {
		this.setNumberOfTokens(this.getNumberOfTokens() - 1);
	}
	
	public void produceToken() {
		this.setNumberOfTokens(this.getNumberOfTokens() + 1);
	}
	
	public int getNumberOfTokens() {
		return numberOfTokens;
	}

	//helper methods
	private boolean isInitalPlace = false;
	private boolean isFinalPlace = false;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setNumberOfTokens(int numberOfTokens) {
		this.numberOfTokens = numberOfTokens;
	}
	public boolean isInitalPlace() {
		return isInitalPlace;
	}
	public void setInitalPlace(boolean isInitalPlace) {
		this.isInitalPlace = isInitalPlace;
	}
	public boolean isFinalPlace() {
		return isFinalPlace;
	}
	public void setFinalPlace(boolean isFinalPlace) {
		this.isFinalPlace = isFinalPlace;
	}

}
