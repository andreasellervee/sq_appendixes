package implementation;


import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.extension.std.XTimeExtension;
import org.deckfour.xes.model.XAttributeMap;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.processmining.models.connections.GraphLayoutConnection;
import org.processmining.models.graphbased.directed.petrinet.PetrinetEdge;
import org.processmining.models.graphbased.directed.petrinet.PetrinetGraph;
import org.processmining.models.graphbased.directed.petrinet.PetrinetNode;
import org.processmining.models.graphbased.directed.petrinet.elements.Place;
import org.processmining.models.graphbased.directed.petrinet.elements.Transition;
import org.processmining.models.graphbased.directed.petrinet.impl.PetrinetFactory;
import org.processmining.models.semantics.petrinet.Marking;
import org.processmining.plugins.pnml.Pnml;

import parsers.PnmlImportUtils;
import parsers.XLogReader;
import petrinet.Event;
import petrinet.EventLog;
import petrinet.PetriNet;
import petrinet.Trace;

public class EntityManager {
	
	public PetriNet getPetriNet(String pnmlFile) throws Exception {
		PnmlImportUtils ut = new PnmlImportUtils();
		File f = new File(pnmlFile);
			InputStream input = new FileInputStream(f);
			Pnml pnml = ut.importPnmlFromStream(input, f.getName(), f.length());
			PetrinetGraph net = PetrinetFactory.newInhibitorNet(pnml.getLabel() + " (imported from " + f.getName() + ")");
			Marking marking = new Marking();
			pnml.convertToNet(net,marking ,new GraphLayoutConnection(net));
			
			PetriNet myPetriNet = new PetriNet();
			
			Collection<Place> places = net.getPlaces();
			// populate our places
			List<petrinet.Place> myPlaces = new ArrayList<petrinet.Place>();
			for (Place place : places) {
				petrinet.Place myPlace = new petrinet.Place();
				
				// to get outgoing edges from a place
				Collection<PetrinetEdge<? extends PetrinetNode, ? extends PetrinetNode>> edgesOutP = net.getOutEdges(place);
				if(edgesOutP.isEmpty()) {
					myPlace.setFinalPlace(true);
				}

				//to get ingoing edges to a place
				// if place does not have any incoming edges then this is the beginning of the PetriNet
				// and gets the inital marking.
				Collection<PetrinetEdge<? extends PetrinetNode, ? extends PetrinetNode>> edgesInP = net.getInEdges(place);
				if (edgesInP.isEmpty()) {
					myPlace.setInitalPlace(true);
					myPlace.setNumberOfTokens(1);
				}
				myPlace.setName(place.getLabel());
				myPlaces.add(myPlace);
			}
			
			Collection<Transition> transitions = net.getTransitions();
			// populate our transition
			List<petrinet.Transition> myTransitions = new ArrayList<petrinet.Transition>();
			for (Transition transition : transitions) {
				petrinet.Transition myTransition = new petrinet.Transition();
				// set the label
				myTransition.setLabel(transition.getLabel());
				// get all the input Places
				Collection<PetrinetEdge<? extends PetrinetNode, ? extends PetrinetNode>> inEdges = net.getInEdges(transition);
				List<petrinet.Place> inputPlaces = new ArrayList<petrinet.Place>();
				for (PetrinetEdge<? extends PetrinetNode, ? extends PetrinetNode> edge : inEdges) {
					for(petrinet.Place myPlace : myPlaces) {
						if (myPlace.getName().equals(edge.getSource().getLabel())) {
							inputPlaces.add(myPlace);
						}
					}
				}
				myTransition.setInputPlaces(inputPlaces);
				
				//get all the output Places
				Collection<PetrinetEdge<? extends PetrinetNode, ? extends PetrinetNode>> edgesOutT = net.getOutEdges(transition);
				List<petrinet.Place> outputPlaces = new ArrayList<petrinet.Place>();
				for (PetrinetEdge<? extends PetrinetNode, ? extends PetrinetNode> edge : edgesOutT) {
					for(petrinet.Place myPlace : myPlaces) {
						if (myPlace.getName().equals(edge.getTarget().getLabel())) {
							outputPlaces.add(myPlace);
						}
					}
				}
				myTransition.setOutputPlaces(outputPlaces);
				myTransitions.add(myTransition);
			}
			
			myPetriNet.setPlaces(myPlaces);
			myPetriNet.setTransitions(myTransitions);
			return myPetriNet;
	}
	
	public EventLog getEventLog(String xesFile) throws Exception {
		XLog log = XLogReader.openLog(xesFile);
		EventLog myEventLog = new EventLog();
		List<Trace> myTraces = new ArrayList<>();
		
		for(XTrace trace : log){
			
			// myStuff
			Trace myTrace = new Trace();
			List<Event> myEventList = new ArrayList<Event>();
			Map<String, Object> myCaseAttributes = myTrace.getCaseAttributes();
			//
			
			XAttributeMap caseAttributes = trace.getAttributes();
			for(String key :caseAttributes.keySet()){
				myCaseAttributes.put(key, caseAttributes.get(key));
			}
			
			// adding the case attributes
			myTrace.setCaseAttributes(myCaseAttributes);
			
			for(XEvent event : trace){
				Event myEvent = new Event();
				Map<String, Object> myEventAttributes = myEvent.getEventAttributes();
				
				String activityName = XConceptExtension.instance().extractName(event);
				myEvent.setName(activityName);
				Date timestamp = XTimeExtension.instance().extractTimestamp(event);
				myEvent.setTimestamp(timestamp);
				
				XAttributeMap eventAttributes = event.getAttributes();
				for(String key : eventAttributes.keySet()){
					myEventAttributes.put(key, eventAttributes.get(key));
				}
				myEvent.setEventAttributes(myEventAttributes);
				myEventList.add(myEvent);
			}
			myTrace.setEvents(myEventList);
			myTraces.add(myTrace);
		}
		Map<String, Trace> something = new HashMap<>();
		Map<String, Integer> traceInstances = new HashMap<>();
		for(Trace trace : myTraces) {
			if (traceInstances.containsKey(trace.toString())) {
				traceInstances.put(trace.toString(), traceInstances.get(trace.toString()) + 1);
			} else {
				traceInstances.put(trace.toString(), 1);
				something.put(trace.toString(), trace);
			}
		}
		
		for(Trace trace : myTraces) {
			trace.setNumberOfInstances(traceInstances.get(trace.toString()));
		}
		
		myEventLog.setTraces(myTraces);
		return myEventLog;
	}

}
