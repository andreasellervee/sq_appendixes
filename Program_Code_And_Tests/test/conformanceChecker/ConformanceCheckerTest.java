package conformanceChecker;

import org.junit.Assert;
import org.junit.Test;

import conformance.ConformanceChecker;
import implementation.EntityManager;
import petrinet.EventLog;
import petrinet.PetriNet;

public class ConformanceCheckerTest {
	
	@Test
	public void testConfromanceChecker() throws Exception {
		EntityManager entityManager = new EntityManager();
		PetriNet petriNet = entityManager.getPetriNet("tests2/test.pnml");
		EventLog eventLog = entityManager.getEventLog("tests2/test.xes");
		ConformanceChecker cc = new ConformanceChecker(eventLog, petriNet);
		Assert.assertEquals(cc.getFitness(), 0.9333d, 1);
		Assert.assertEquals(cc.getSimpleBehavAppropr(), 0.9354d, 1);
		Assert.assertEquals(cc.getSimpleStructAppropr(), 0.5263d, 1);
	}
}
