package petrinet;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

public class TransitionTest {
	
	@Test
	public void testGetName() {
		Transition t = new Transition();
		t.setLabel("T1");
		Assert.assertEquals("T1", t.getName());
	}
	
	@Test
	public void testIsEnabled() {
		Transition t = new Transition();
		t.setLabel("T1");
		t.setTrace(null);
		Place p1 = getPlace("P1");
		p1.setNumberOfTokens(1);
		Place p2 = getPlace("P2");
		p2.setNumberOfTokens(1);
		t.setInputPlaces(Arrays.asList(p1, p2));
		Assert.assertTrue(t.isEnabled());
	}
	
	@Test
	public void testIsNotEnabled() {
		Transition t = new Transition();
		t.setLabel("T1");
		t.setTrace(null);
		Place p1 = getPlace("P1");
		p1.setNumberOfTokens(1);
		Place p2 = getPlace("P2");
		p2.setNumberOfTokens(0);
		t.setInputPlaces(Arrays.asList(p1, p2));
		Assert.assertFalse(t.isEnabled());
	}
	
	@Test
	public void testIsEnabledWithTrace() {
		Trace trace = new Trace();
		Transition t = new Transition();
		t.setLabel("T1");
		t.setTrace(trace);
		Place p1 = getPlace("P1");
		p1.setNumberOfTokens(1);
		Place p2 = getPlace("P2");
		p2.setNumberOfTokens(1);
		t.setInputPlaces(Arrays.asList(p1, p2));
		Assert.assertTrue(t.isEnabled());
	}
	
	@Test
	public void testIsNotEnabledWithTrace() {
		Trace trace = new Trace();
		Transition t = new Transition();
		t.setLabel("T1");
		t.setTrace(trace);
		Place p1 = getPlace("P1");
		p1.setNumberOfTokens(1);
		Place p2 = getPlace("P2");
		p2.setNumberOfTokens(0);
		t.setInputPlaces(Arrays.asList(p1, p2));
		Assert.assertFalse(t.isEnabled());
	}
	
	@Test
	public void testConsumeEvent() {
		Trace trace = new Trace();
		Transition t = new Transition();
		t.setLabel("T1");
		t.setTrace(trace);
		Place p1 = getPlace("P1");
		p1.setNumberOfTokens(1);
		Place p2 = getPlace("P2");
		p2.setNumberOfTokens(1);
		t.setInputPlaces(Arrays.asList(p1, p2));
		Place p3 = getPlace("P3");
		p3.setNumberOfTokens(0);
		t.setOutputPlaces(Arrays.asList(p3));
		
		t.consumeEvent();
		
		Assert.assertTrue(p1.getNumberOfTokens() == 0);
		Assert.assertTrue(p2.getNumberOfTokens() == 0);
		Assert.assertTrue(p3.getNumberOfTokens() == 1);
	}
	
	private Place getPlace(String name) {
		Place p = new Place();
		p.setName(name);
		return p;
	}

}
