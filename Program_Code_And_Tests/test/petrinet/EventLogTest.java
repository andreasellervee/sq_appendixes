package petrinet;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;

import org.junit.Assert;
import org.junit.Test;

public class EventLogTest {
	
	@Test
	public void testGetDistinctLabels() {
		EventLog el = getEventLog();
		Assert.assertEquals(el.getDistinctLabels(), new HashSet<String>(Arrays.asList("A", "B", "C")));
	}
	
	private EventLog getEventLog() {
		EventLog el = new EventLog();
		Trace trace = new Trace();
		Event e1 = getEvent("A");
		Event e2 = getEvent("B");
		Event e3 = getEvent("C");
		Event e4 = getEvent("A");
		Event e5 = getEvent("C");
		trace.setCaseId(1);
		trace.setEvents(Arrays.asList(e1, e2, e3, e4, e5));
		el.setTraces(Arrays.asList(trace));
		return el;
		
	}

	private Event getEvent(String name) {
		Event e = new Event();
		e.setName(name);
		e.setTimestamp(new Date());
		return e;
	}

}
