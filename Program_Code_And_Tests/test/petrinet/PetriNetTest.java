package petrinet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import junit.framework.Assert;

public class PetriNetTest {
	
	@Test
	public void testReset() {
		PetriNet petriNet = getTestData();
		for(Place p : petriNet.getPlaces()) {
			if (p.getName().equals("P1")) {
				Assert.assertTrue(p.getNumberOfTokens() == 0);
			}
		}
		
		petriNet.reset();
		
		for(Place p : petriNet.getPlaces()) {
			if (p.getName().equals("P1")) {
				Assert.assertTrue(p.getNumberOfTokens() == 1);
			}
		}
	}
	
	private PetriNet getTestData() {
		PetriNet net = new PetriNet();
		
		List<Place> places = new ArrayList<>();
		Place p1 = getPlace("P1");
		p1.setInitalPlace(true);
		Place p2 = getPlace("P2");
		Place p3 = getPlace("P3");
		p3.setFinalPlace(true);
		p3.setNumberOfTokens(1);
		places.add(p1);
		places.add(p2);
		places.add(p3);
		
		List<Transition> transitions = new ArrayList<>();
		Transition t1 = getTransition("T1");
		t1.setInputPlaces(Arrays.asList(p1));
		t1.setOutputPlaces(Arrays.asList(p2));
		transitions.add(t1);
		
		Transition t2 = getTransition("T2");
		t2.setInputPlaces(Arrays.asList(p2));
		t2.setOutputPlaces(Arrays.asList(p3));
		transitions.add(t2);
		
		net.setPlaces(places);
		net.setTransitions(transitions);
		
		return net;
	}
	
	private Transition getTransition(String label) {
		Transition t = new Transition();
		t.setLabel(label);
		return t;
	}
	
	private Place getPlace(String name) {
		Place p = new Place();
		p.setName(name);
		return p;
	}

}
