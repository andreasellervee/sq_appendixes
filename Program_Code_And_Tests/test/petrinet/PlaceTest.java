package petrinet;

import org.junit.Assert;
import org.junit.Test;

public class PlaceTest {
	
	@Test
	public void testIsFinalPlace() {
		Place p = new Place();
		p.setName("P1");
		p.setFinalPlace(true);
		Assert.assertTrue(p.isFinalPlace());
	}

}
